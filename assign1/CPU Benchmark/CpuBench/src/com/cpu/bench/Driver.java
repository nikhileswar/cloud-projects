package com.cpu.bench;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Driver {

	public static long startTime=0,endTime=0;
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		double iops=0,flops=0;
		System.out.println("Measuring IOPS");
		
		for(int i=0;i<4;i++){
		
		switch(i){
		
		case 0: System.out.println("Executing with 1 Thread");
	      		ExecutorService executor = Executors.newFixedThreadPool(1);
	      		startTime=System.currentTimeMillis();
	      		Runnable worker = new CpuOperations();
	      		executor.execute(worker);

	      		executor.shutdown();

	      		while (!executor.isTerminated()) {
	      		}

	      		endTime=System.currentTimeMillis();
	      		iops=(double)4*(100000)*(100000)*(1000)/(endTime-startTime);
	      		iops=iops/(1000000000);

	      		System.out.println("Iops for 1 Thread is "+iops+" Giga Iops");
	      		break;
	      		
		case 1: System.out.println("Executing with 2 Thread");
  				ExecutorService executor1 = Executors.newFixedThreadPool(2);
  				startTime=System.currentTimeMillis();
  				Runnable worker1 = new CpuOperations();
  				executor1.execute(worker1);

  				executor1.shutdown();

  				while (!executor1.isTerminated()) {
  				}

  				endTime=System.currentTimeMillis();
  				iops=(double)4*(100000)*(100000)*(1000)/(endTime-startTime);
  				iops=iops/(1000000000);

  				System.out.println("Iops for 2 Threads is "+iops+" Giga Iops");
  				break;
  				
		case 2: System.out.println("Executing with 8 Threads");
				ExecutorService executor2 = Executors.newFixedThreadPool(4);
				startTime=System.currentTimeMillis();
				Runnable worker2 = new CpuOperations();
				executor2.execute(worker2);

				executor2.shutdown();

				while (!executor2.isTerminated()) {
				}

				endTime=System.currentTimeMillis();
				iops=(double)4*(100000)*(100000)*(1000)/(endTime-startTime);
				iops=iops/(1000000000);

				System.out.println("Iops for 4 Threads is "+iops+" Giga Iops");
				break;
				
		case 3:	System.out.println("Executing with 8 Threads");
				ExecutorService executor3 = Executors.newFixedThreadPool(4);
				startTime=System.currentTimeMillis();
				Runnable worker3 = new CpuOperations();
				executor3.execute(worker3);

				executor3.shutdown();

				while (!executor3.isTerminated()) {
				}

				endTime=System.currentTimeMillis();
				iops=(double)4*(100000)*(100000)*(1000)/(endTime-startTime);
				iops=iops/(1000000000);

				System.out.println("Iops for 8 Threads is "+iops+" Giga Iops");
				break;
			
			}
			

	      
		}
		
		System.out.println("Measuring FLOPS");
		
		for(int i=0;i<4;i++){
			
		switch(i){
		
		case 0: System.out.println("Executing with 1 Thread");
	      		ExecutorService executor = Executors.newFixedThreadPool(1);
	      		startTime=System.currentTimeMillis();
	      		Runnable worker = new CpuFloatOperations();
	      		executor.execute(worker);

	      		executor.shutdown();

	      		while (!executor.isTerminated()) {
	      		}

	      		endTime=System.currentTimeMillis();
	      		flops=(double)5*(100000)*(100000)*(1000)/(endTime-startTime);
	      		flops=flops/(1000000000);

	      		System.out.println("Iops for 1 Thread is "+flops+" Giga FLOPS");
	      		break;
	      		
		case 1: System.out.println("Executing with 2 Thread");
  				ExecutorService executor1 = Executors.newFixedThreadPool(2);
  				startTime=System.currentTimeMillis();
  				Runnable worker1 = new CpuFloatOperations();
  				executor1.execute(worker1);

  				executor1.shutdown();

  				while (!executor1.isTerminated()) {
  				}

  				endTime=System.currentTimeMillis();
  				flops=(double)5*(100000)*(100000)*(1000)/(endTime-startTime);
  				flops=flops/(1000000000);

  				System.out.println("Iops for 2 Threads is "+flops+" Giga FLOPS");
  				break;
  				
		case 2: System.out.println("Executing with 8 Threads");
				ExecutorService executor2 = Executors.newFixedThreadPool(4);
				startTime=System.currentTimeMillis();
				Runnable worker2 = new CpuFloatOperations();
				executor2.execute(worker2);

				executor2.shutdown();

				while (!executor2.isTerminated()) {
				}

				endTime=System.currentTimeMillis();
				flops=(double)5*(100000)*(100000)*(1000)/(endTime-startTime);
				flops=flops/(1000000000);

				System.out.println("Iops for 4 Threads is "+flops+" Giga FLOPS");
				break;
				
		case 3:	System.out.println("Executing with 8 Threads");
				ExecutorService executor3 = Executors.newFixedThreadPool(4);
				startTime=System.currentTimeMillis();
				Runnable worker3 = new CpuFloatOperations();
				executor3.execute(worker3);

				executor3.shutdown();

				while (!executor3.isTerminated()) {
				}

				endTime=System.currentTimeMillis();
				flops=(double)5*(100000)*(100000)*(1000)/(endTime-startTime);
				flops=flops/(1000000000);

				System.out.println("Iops for 8 Threads is "+flops+" Giga FLOPS");
				break;
			
			}
			

	      
		}
		
		
		
		

	}

}
