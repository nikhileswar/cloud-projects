import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;



public class SQSExec {
	public static String instanceId;
	public static String bucketName     = "mymov";
	public static String keyName        = "mymovie.mpg";
	public static void main(String[] args) throws Exception {
		int count=5,i=0;
		long time=0;
		boolean check=true;
		new File("animoto").mkdir();
		AmazonS3 s3client = new AmazonS3Client(new ProfileCredentialsProvider());
		HashMap<String, Integer> mapPool = new HashMap<String, Integer>();
		List<String> instanceIds = new ArrayList<String>();
		AWSSQS.init();
		String SQSURL="https://sqs.us-west-2.amazonaws.com/521499112865/Animoto";
		String SQSRES="https://sqs.us-west-2.amazonaws.com/521499112865/Result";
		//Parse Idle Time
		DynamoDB.init();
			while(check==true){
				
				String command=AWSSQS.readDelMessage(SQSURL);
				if(command!=null){
					mapPool.clear();
					String[] commandE=command.split(";");
						mapPool.put(command, Integer.parseInt(commandE[1]));
						ThreadExecutor.init(mapPool.size());
						ThreadExecutor.ExecuteTasks(mapPool);
				}
				else{
					Thread.sleep(5*1000);
				}
				if(DynamoDB.retrieveCountRecords("Animoto")==0){
					check=false;
				}
			}	
			Thread.sleep(20*1000);
			Runtime run = Runtime.getRuntime();
			Process pr = run.exec("ffmpeg -f image2 -i /home/ubuntu/animoto/img%d.jpg mov.mpg");
			pr.waitFor();
			Thread.sleep(10*1000);
			File file = new File("mov.mpg");
			if(file.exists()) { 
				s3client.putObject(new PutObjectRequest(
		                 bucketName, keyName, file).withCannedAcl(CannedAccessControlList.PublicRead));
				String s3URL="https://s3-us-west-2.amazonaws.com/mymov/"+keyName;
				AWSSQS.sendMessage(SQSRES, s3URL);
			}

            Thread.sleep(10*1000);
			String instanceId=getInstanceId();
			instanceIds.add(instanceId);
			AWSSQS.TerminateInstance(instanceIds);	
		
	}
	
	public static String getInstanceId() throws IOException{
		String Id = "";
		String inputLine="";
		URL EC2MetaData = new URL("http://169.254.169.254/latest/meta-data/instance-id");
		URLConnection EC2MD = EC2MetaData.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(EC2MD.getInputStream()));
				while ((inputLine = in.readLine()) != null)
				{	
					Id = inputLine;
					System.out.println(Id);
				}
				in.close();
				return Id;
	}


	

}
