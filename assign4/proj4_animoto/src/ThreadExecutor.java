

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class ThreadExecutor {
	public static ExecutorService executor;
	public static String RESURL="https://sqs.us-west-2.amazonaws.com/521499112865/ResultQueue";
	
	public static void init(int n) {
		executor = Executors.newFixedThreadPool(n);
	}

public static HashMap<Integer, Integer> ExecuteTasks(HashMap<String , Integer> tasks) throws UnknownHostException{
	final HashMap<Integer, Integer> results = new HashMap<Integer, Integer>();
	
		for(Entry<String, Integer> entry: tasks.entrySet()){
			final String task=entry.getKey();
			final int id=entry.getValue();

			final int tasknum=entry.getValue();
			InetAddress IP=InetAddress.getLocalHost();
			final String ip=IP.getHostAddress();
			Future te=executor.submit(new Runnable(){
			
				@Override
				public void run() {
					// TODO Auto-generated method stub
					try {
						String[] URL=task.split(";");
						String fileName = "/home/ubuntu/animoto/img"; 
						 URL link = new URL(URL[2]); 
						
						 InputStream in = new BufferedInputStream(link.openStream());
						 ByteArrayOutputStream Bout = new ByteArrayOutputStream();
						 byte[] buf = new byte[1024];
						 int n = 0;
						 while (-1!=(n=in.read(buf)))
						 {
							 Bout.write(buf, 0, n);
						 }
						 Bout.close();
						 in.close();
						 byte[] tempBuffer = Bout.toByteArray();
						 FileOutputStream fs = new FileOutputStream(fileName+id+".jpg");
						 fs.write(tempBuffer);
						 fs.close();
						results.put(tasknum, 0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						results.put(tasknum, 1);
					}
					
				}
				
				
			});
			
		}
		
		return results;
		
	}

public static void StopExec() {
	System.out.println("Shutting Down Executor");
	executor.shutdown();
}
	
}
