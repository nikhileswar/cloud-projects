package com.servlet;
import java.util.List;
import java.util.Map.Entry;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;


public class AWSSQS {
	
	public static AWSCredentials credentials = null;
	public static AmazonSQS sqs=null;
	static String myQueueUrl=null;
	public static void init(){
		
		try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (C:\\Users\\nikhil\\.aws\\credentials), and is in valid format.",
                    e);
        }
		sqs = new AmazonSQSClient(credentials);
	    Region usWest2 = Region.getRegion(Regions.US_WEST_2);
	    sqs.setRegion(usWest2);
	}
	
	public static String CreateQueue(String Qname){
        System.out.println("Creating a new SQS queue :"+Qname+".\n");
        CreateQueueRequest createQueueRequest = new CreateQueueRequest(Qname);
        myQueueUrl = sqs.createQueue(createQueueRequest).getQueueUrl();
        return myQueueUrl;
	}
	public static void sendMessage(String SQSURL,String message){
		
		sqs.sendMessage(new SendMessageRequest(SQSURL, message));
	}
	
	public static String readMessage(String SQSURL){
		String command=null;
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(SQSURL);
		receiveMessageRequest.setWaitTimeSeconds(10);
        List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
        if(messages.size()>0){
        command=messages.get(0).getBody();
        }
		return command;
	}
	
	public static void deleteMessage(String SQSURL){
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(SQSURL);
        List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();

        String messageRecieptHandle = messages.get(0).getReceiptHandle();
        sqs.deleteMessage(new DeleteMessageRequest(SQSURL, messageRecieptHandle));
        System.out.println("message Deleted");
        
	}
	
	public static void deleteQueue(String SQSURL){
		
        System.out.println("Deleting the Task queue.\n");
        sqs.deleteQueue(new DeleteQueueRequest(SQSURL));	
	}
	
	public static int computeSizeQueue(List<Message> messages){
		int size=0;
        for (Message message : messages) {
        	size++;
        }
        return size;
		
	}
	

}
