package com.servlet;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

public class upload extends HttpServlet{

	public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
			
		FileItemFactory factory = new DiskFileItemFactory();
		 
		ServletFileUpload fileUpload = new ServletFileUpload();
		ServletOutputStream out = null;
		
	    String fileContents=null;
	    InputStream is = null;
	    FileItemIterator items = null;
		try {
			items = fileUpload.getItemIterator(req);

			
			while (items.hasNext()) {
				FileItemStream item = (FileItemStream)items.next();
				if (!item.isFormField()) {
				    is = item.openStream();
				    fileContents = IOUtils.toString(is, "UTF-8");
				    
				    String[] jobs=fileContents.split("\n");
				    
				    AWSSQS.init();
					DynamoDB.init();
					DynamoDB.CreateTable("Animoto");				
					String SQSURL=AWSSQS.CreateQueue("Animoto");
					String SQSRES=AWSSQS.CreateQueue("Result");
					
					
					for(int i=0;i<jobs.length;i++){
						 
						 AWSSQS.sendMessage(SQSURL, "TaskNum;"+i+";"+jobs[i]);
						 //add to dynamo DB
						 DynamoDB.InsertRecord("Animoto", i, jobs[i], false);

					 }
					
					AWSSpotinstances.LaunchSpotInstances(1);
					
					String resURL=AWSSQS.readMessage(SQSRES);
					while(resURL==null){
						res.getWriter().println("Waiting for job to complete");
						System.out.println("Waiting for job to complete");
						Thread.sleep(5*1000);
						resURL=AWSSQS.readMessage(SQSRES);
					}
					res.getWriter().println("Link to Movie is "+resURL);
					
				}
				
				
			}
			
		} catch (FileUploadException e) {

			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		


	}
	
}
