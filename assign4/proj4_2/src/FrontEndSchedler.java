

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;


public class FrontEndSchedler {
	public static long startTime=0,endTime=0;
	public static void main(String[] args) throws Exception {
		String pattern="^[A-z]+\\s{1}\\-{1}[A-z]{1}\\s{1}([0-9]+)\\s{1}\\-{1}[A-z]{2}\\s{1}([0-9]+)$";
		String Command=null;
		String ip_port=null;
		int port;
		int count=0;
		int flag=0;
		boolean IsRunning=true;
		Pattern r = Pattern.compile(pattern);
		Scanner s=new Scanner(System.in);
		ServerSocket mySocket;
		Socket server;
		DataInputStream input;
		DataOutputStream output;
		HashMap<String, Integer> mapPool = new HashMap<String, Integer>();
		if((Command=s.nextLine())!=null){
			Matcher m = r.matcher(Command);
			if (m.find( )){
				
				String[] tokens=Command.split(" ");
				
				ip_port=tokens[2];
				port=Integer.parseInt(ip_port);

				
				mySocket = new ServerSocket(port);

				while(IsRunning){
				 server=mySocket.accept();
				 input = new DataInputStream(server.getInputStream());
				 String data_recieved=input.readUTF();
				 if(data_recieved.equalsIgnoreCase("end")){
					 IsRunning=false;
					 break;
				 }
				 String[] jobs=data_recieved.split(";");
				 for(int i=0;i<jobs.length;i++){
					 mapPool.put(jobs[i], i);
				 }
				 if(tokens[3].equalsIgnoreCase("-lw")){
					 
					 startTime=System.currentTimeMillis();
					 ThreadExecutor.init(Integer.parseInt(tokens[4]));
					 HashMap<Integer,Integer> results=ThreadExecutor.ExecuteTasks(mapPool);
					 output = new DataOutputStream(server.getOutputStream());
					 for(Entry<Integer, Integer> entry: results.entrySet()){
						 String res="Task "+entry.getKey()+" "+entry.getValue();
						 System.out.println(res);
						 output.writeUTF(res);
					 }
					 output.close();
					 ThreadExecutor.StopExec();
					 endTime=System.currentTimeMillis();
				 }
				 
				 if(tokens[3].equalsIgnoreCase("-rw")){
					 AWSSQS.init();
					 DynamoDB.init();
					 String workerCommand="";
					 startTime=System.currentTimeMillis();
					 if(flag==0){
						 String SQSIDLE=AWSSQS.CreateQueue("IdleTime");
						 System.out.println("Please specify Worker idle Time in worker -i <time> format");
						 workerCommand =s.nextLine();
						 AWSSQS.sendMessage(SQSIDLE, workerCommand);
						 flag++;
					 }
					 // Create Task Queue
					 int numJobs=jobs.length;
					 
					 DynamoDB.CreateTable("TasksTable");				
					 String SQSURL=AWSSQS.CreateQueue("TaskQueue");
					 AWSSQS.CreateQueue("ResultQueue");
					 System.out.println("Sending Jobs to SQS");
					 for(int i=0;i<numJobs;i++){
						 
						 AWSSQS.sendMessage(SQSURL, "TaskNum:"+count+":"+jobs[i]);
						 //add to dynamo DB
						 DynamoDB.InsertRecord("TasksTable", count, jobs[i], false);
						 count++;
							if(CheckpowerOfTwo(count)){
								AWSSpotinstances.LaunchSpotInstances(FrontEndSchedler.Dynamicprovisioning(count));
							}
						 
					 }
					 int left_jobs=DynamoDB.retrieveCountRecords("TasksTable");
					 if(left_jobs>0){
						 
						 AWSSpotinstances.LaunchSpotInstances(FrontEndSchedler.Dynamicprovisioning(left_jobs));
						 
					 }
					 
					 //AWSSpotinstances.LaunchSpotInstances(2);
					 
					 endTime=System.currentTimeMillis();
				 }
				 
				}

			}
			else{
				
				System.out.println("Invalid Command!");
			}	
		}
		else{
			
			System.out.println("Nothing to Read");
		}
		
		s.close();

	}
	public static int Dynamicprovisioning(int number){
		int number_of_workers=number/16;
		int num_of_working_instances=AWSSpotinstances.getListOfInsances();
		if(number_of_workers<1){
			number_of_workers=1;
		}
		if(number_of_workers>32){
			number_of_workers=32;
		}
		if(num_of_working_instances>=number_of_workers){
			number_of_workers=0;
		}
		else{
			number_of_workers=number_of_workers-num_of_working_instances;
		}
		
	return number_of_workers;
		
	}
	
	public static boolean CheckpowerOfTwo(int number){
		
		int temp=1;
		while(number>=temp){
			
			if(number==temp){
				return true;
			}
			
			temp=temp*2;
		}
		return false;	
	}

}
