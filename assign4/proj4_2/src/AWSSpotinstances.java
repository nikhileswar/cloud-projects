import java.util.ArrayList;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.*;


public class AWSSpotinstances {
	
	public static AmazonEC2Client amazonEC2Client;
	public static ArrayList<String> instanceIds ;
	
public static void CreateSecurityGroup(){
		
		// Creating Security Group
		CreateSecurityGroupRequest createSecurityGroupRequest = new CreateSecurityGroupRequest();    	
		createSecurityGroupRequest.withGroupName("Proj4SecurityGroup").withDescription("Proj4 Security Group");
		CreateSecurityGroupResult createSecurityGroupResult = amazonEC2Client.createSecurityGroup(createSecurityGroupRequest);
		
		// Allow Inbound Traffic
		
		IpPermission ipPermission = new IpPermission();
			    	
			ipPermission.withIpRanges("111.111.111.111/32", "150.150.150.150/32").withIpProtocol("tcp").withFromPort(22).withToPort(22);
			AuthorizeSecurityGroupIngressRequest authorizeSecurityGroupIngressRequest =new AuthorizeSecurityGroupIngressRequest();
				    	
				authorizeSecurityGroupIngressRequest.withGroupName("Proj4SecurityGroup").withIpPermissions(ipPermission);
				amazonEC2Client.authorizeSecurityGroupIngress(authorizeSecurityGroupIngressRequest);
	}
	public static void CreateKeyPair(){
		
		CreateKeyPairRequest createKeyPairRequest = new CreateKeyPairRequest();
		createKeyPairRequest.withKeyName("proj4");
		CreateKeyPairResult createKeyPairResult = amazonEC2Client.createKeyPair(createKeyPairRequest);
		KeyPair keyPair = new KeyPair();

		keyPair = createKeyPairResult.getKeyPair();

		String privateKey = keyPair.getKeyMaterial();

	}	
	
	public static ArrayList<String> LaunchSpotInstances( int numInstances) throws InterruptedException{
		if(numInstances>0){
		AWSCredentials credentials = null;
        try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (C:\\Users\\nikhil\\.aws\\credentials), and is in valid format.",
                    e);
        }

        // Create the AmazonEC2Client object so we can call various APIs.
        AmazonEC2 ec2 = new AmazonEC2Client(credentials);
        Region usWest2 = Region.getRegion(Regions.US_WEST_2);
        ec2.setRegion(usWest2);

        // Initializes a Spot Instance Request
        RequestSpotInstancesRequest requestRequest = new RequestSpotInstancesRequest();

        // Request 1 x t1.micro instance with a bid price of $0.03.
        requestRequest.setSpotPrice("0.03");
        requestRequest.setInstanceCount(Integer.valueOf(numInstances));


        LaunchSpecification launchSpecification = new LaunchSpecification();
        launchSpecification.setImageId("ami-bff8af8f");
        launchSpecification.setInstanceType("t1.micro");

        // Add the security group to the request.
        launchSpecification.withSecurityGroups("Proj4SecurityGroup");
        launchSpecification.withKeyName("node1");
        // Add the launch specifications to the request.
        requestRequest.setLaunchSpecification(launchSpecification);

        //============================================================================================//
        //=========================== Getting the Request ID from the Request ========================//
        //============================================================================================//

 
        RequestSpotInstancesResult requestResult = ec2.requestSpotInstances(requestRequest);
        List<SpotInstanceRequest> requestResponses = requestResult.getSpotInstanceRequests();


        ArrayList<String> spotInstanceRequestIds = new ArrayList<String>();


        for (SpotInstanceRequest requestResponse : requestResponses) {
            System.out.println("Created Spot Request: "+requestResponse.getSpotInstanceRequestId());
            spotInstanceRequestIds.add(requestResponse.getSpotInstanceRequestId());
        }
        System.out.println("Getting Started Up.Please Wait...");
        boolean anyOpen;


         instanceIds = new ArrayList<String>();

        do {

            DescribeSpotInstanceRequestsRequest describeRequest = new DescribeSpotInstanceRequestsRequest();
            describeRequest.setSpotInstanceRequestIds(spotInstanceRequestIds);


            anyOpen=false;

            try {

                DescribeSpotInstanceRequestsResult describeResult = ec2.describeSpotInstanceRequests(describeRequest);
                List<SpotInstanceRequest> describeResponses = describeResult.getSpotInstanceRequests();


                for (SpotInstanceRequest describeResponse : describeResponses) {
                        if (describeResponse.getState().equals("open")) {
                            anyOpen = true;
                            break;
                        }

                        instanceIds.add(describeResponse.getInstanceId());
                }
            } catch (AmazonServiceException e) {
                anyOpen = true;
            }

            try {
                // Sleep for 30 seconds.
                Thread.sleep(30*1000);
            } catch (Exception e) {
            	
            }
            System.out.println("Spot Request still Open. Please Wait");
        } while (anyOpen);
        //waitForInstanceState(ec2,instanceIds, InstanceStateName.Running);
        return instanceIds;
		}
		else{
			//Do nothing and exit
			return null;
		}

	}
	public static void waitForInstanceState(AmazonEC2 ec2,List<String> instancesId, InstanceStateName state) {
        int numAchievedState = 0;

        while (numAchievedState != instancesId.size()) {
        	System.out.println("Waiting for instances to start up");
            try {
                Thread.sleep(15000);
            } catch (InterruptedException ex) {
                
            }

            numAchievedState = 0;

            DescribeInstancesRequest describeInstance = new DescribeInstancesRequest().withInstanceIds(instancesId);
            DescribeInstancesResult describeResult = ec2.describeInstances(describeInstance);
            List<Reservation> reservations = describeResult.getReservations();

            //different instances might be in different reservation requests
            //so we need to traverse those
            for (Reservation reservation : reservations) {
                List<Instance> instances = reservation.getInstances();
                for (Instance instance : instances) {
                    if (instance.getState().getName().equals(state.toString())) {
                        numAchievedState++;
                    }
                }
            }
        }
    }
	
	public static int getListOfInsances(){
		AWSCredentials credentials = null;
		int count=0;
        try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (C:\\Users\\nikhil\\.aws\\credentials), and is in valid format.",
                    e);
        }
        AmazonEC2 ec2 = new AmazonEC2Client(credentials);
        Region usWest2 = Region.getRegion(Regions.US_WEST_2);
        ec2.setRegion(usWest2);
		DescribeInstancesRequest describeInstance = new DescribeInstancesRequest();
        DescribeInstancesResult describeResult = ec2.describeInstances(describeInstance);
        List<Reservation> reservations = describeResult.getReservations();
        
        for (Reservation reservation : reservations) {
            List<Instance> instances = reservation.getInstances();
            for (Instance instance : instances) {
                if (instance.getState().getName().equalsIgnoreCase("running")) {
                	count++;
                }
            }
        }
        
        return (count-2);
	}
	
	
}
