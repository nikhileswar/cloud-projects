import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Client {

	public static void main(String[] args) throws IOException {
		
		String pattern="^[A-z]+\\s{1}\\-{1}[A-z]{1}\\s{1}([.0-9A-z:]+)\\s{1}\\-{1}[A-z]{1}\\s{1}([A-z.]+)$";
		String Command=null,temp=null;
		String ip_port=null;
		int port;
		String workload_file=null;
		Pattern r = Pattern.compile(pattern);
		Scanner s=new Scanner(System.in);
		Socket mySocket;
		DataInputStream input;
		DataOutputStream output;
		while((Command=s.nextLine())!=null){
			Matcher m = r.matcher(Command);
			if (m.find( )){
				
				String[] tokens=Command.split(" ");
				
				ip_port=tokens[2];
				String[] ipAddress=ip_port.split(":");
				port=Integer.parseInt(ipAddress[1]);
				workload_file=tokens[4];
				String data="";
				File file=new File(workload_file);
				BufferedReader br=new BufferedReader(new FileReader(file));
				
				while((temp=br.readLine())!=null){
					
					data=data+temp+";";
				}
				br.close();

				mySocket =new Socket(ipAddress[0],port);
				 output = new DataOutputStream(mySocket.getOutputStream());
				 output.writeUTF(data);
				 input = new DataInputStream(mySocket.getInputStream());
				 output.close();
				 input.close();
				 mySocket.close();

			}
			else{
				String[] tokens=Command.split(" ");
				ip_port=tokens[2];
				String[] ipAddress=ip_port.split(":");
				port=Integer.parseInt(ipAddress[1]);
				
				String data=tokens[3];
				mySocket =new Socket(ipAddress[0],port);
				 output = new DataOutputStream(mySocket.getOutputStream());
				 output.writeUTF(data);
				 output.close();
				 mySocket.close();
				
			}	
		}
	
		s.close();
		
	}

}
