import java.util.HashMap;
import java.util.Map;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.amazonaws.services.dynamodbv2.util.Tables;
import com.amazonaws.services.dynamodbv2.model.*;


public class DynamoDB {
	public static AmazonDynamoDBClient dynamoDB;
    public static void init() throws Exception {

        AWSCredentials credentials = null;
        try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (C:\\Users\\nikhil\\.aws\\credentials), and is in valid format.",
                    e);
        }
        dynamoDB = new AmazonDynamoDBClient(credentials);
        Region usWest2 = Region.getRegion(Regions.US_WEST_2);
        dynamoDB.setRegion(usWest2);
    }
    
    public static void CreateTable(String tableName){
    	
    	// Create table if it does not exist yet
    	
    	 if (Tables.doesTableExist(dynamoDB, tableName)) {
             System.out.println("Table " + tableName + " Exists");
         } else {
             // Create a table with a primary hash key named 'name', which holds a string
             CreateTableRequest createTableRequest = new CreateTableRequest().withTableName(tableName)
                 .withKeySchema(new KeySchemaElement().withAttributeName("TaskNum").withKeyType(KeyType.HASH))
                 .withAttributeDefinitions(new AttributeDefinition().withAttributeName("TaskNum").withAttributeType(ScalarAttributeType.N))
                 .withProvisionedThroughput(new ProvisionedThroughput().withReadCapacityUnits(1L).withWriteCapacityUnits(1L));
                 TableDescription createdTableDescription = dynamoDB.createTable(createTableRequest).getTableDescription();
             System.out.println("Created Table: " + createdTableDescription);

             // Wait for it to become active
             System.out.println("Waiting for " + tableName + " to become ACTIVE...");
             Tables.waitForTableToBecomeActive(dynamoDB, tableName);
         }
    	
    }
    
    public static void InsertRecord(String tableName,int TaskNum,String Task,boolean IsRead){
    	
        Map<String, AttributeValue> item = newItem(TaskNum,Task,IsRead);
        PutItemRequest putItemRequest = new PutItemRequest(tableName, item);
        PutItemResult putItemResult = dynamoDB.putItem(putItemRequest);
        System.out.println("Result: " + putItemResult);
    	
    }
    
    private static Map<String, AttributeValue> newItem(int TaskNum,String Task, Boolean IsRead) {
        Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
        item.put("TaskNum", new AttributeValue().withN(Integer.toString(TaskNum)));
        item.put("Task", new AttributeValue(Task));
        item.put("IsRead", new AttributeValue().withBOOL(IsRead));
        return item;
    }
    public static void UpdateRecord(String tableName,int taskNum){
    	
        try {
            

            HashMap<String, AttributeValue> key = new HashMap<String, AttributeValue>();
			key.put("TaskNum", new AttributeValue().withN(Integer.toString(taskNum)));

            // Specify the desired price (25.00) and also the condition (price = 20.00)
           
    		Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
    		expressionAttributeValues.put(":val1", new AttributeValue().withBOOL(true)); 
    		expressionAttributeValues.put(":val2", new AttributeValue().withN(Integer.toString(taskNum))); 
            
            ReturnValue returnValues = ReturnValue.ALL_NEW;

            UpdateItemRequest updateItemRequest = new UpdateItemRequest()
                .withTableName(tableName)
                .withKey(key)
                .withUpdateExpression("set IsRead = :val1")
                .withConditionExpression("TaskNum = :val2")
                .withExpressionAttributeValues(expressionAttributeValues)
                .withReturnValues(returnValues);

            UpdateItemResult result = dynamoDB.updateItem(updateItemRequest);
            
            // Check the response.
            System.out.println("Printing item after conditional update to new attribute...");
            System.out.println(result.getAttributes());            
        } catch (ConditionalCheckFailedException cse) {
            // Reload object and retry code.
            System.err.println("Conditional check failed in " + tableName);
        } catch (AmazonServiceException ase) {
            System.err.println("Error updating item in " + tableName);
        }  
    	
    }
    
    public static boolean retrievenUpdateRecord(String tableName,int taskNum){
    	
    	 HashMap<String, AttributeValue> key = new HashMap<String, AttributeValue>();
			key.put("TaskNum", new AttributeValue().withN(Integer.toString(taskNum)));
			
			GetItemRequest getItemRequest = new GetItemRequest()
            .withTableName(tableName)
            .withKey(key)
            .withProjectionExpression("IsRead");
			
			   GetItemResult result = dynamoDB.getItem(getItemRequest);

	            // Check the response.
	            Map<String, AttributeValue>  res= new HashMap<String, AttributeValue>(); 
	            res=result.getItem();
	            String resultV=res.get("IsRead").toString();
	            resultV = resultV.replace("}", "");
	            String[] IsReadS=resultV.split(": ");
	            boolean IsRead= Boolean.valueOf(IsReadS[1]) ;
	            System.out.println(IsRead); 
	            
	            if(IsRead==false){
	    		Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
	    		expressionAttributeValues.put(":val1", new AttributeValue().withBOOL(true)); 
	    		expressionAttributeValues.put(":val2", new AttributeValue().withN(Integer.toString(taskNum))); 
	            
	            ReturnValue returnValues = ReturnValue.ALL_NEW;

	            UpdateItemRequest updateItemRequest = new UpdateItemRequest()
	                .withTableName(tableName)
	                .withKey(key)
	                .withUpdateExpression("set IsRead = :val1")
	                .withConditionExpression("TaskNum = :val2")
	                .withExpressionAttributeValues(expressionAttributeValues)
	                .withReturnValues(returnValues);

	            UpdateItemResult uResult = dynamoDB.updateItem(updateItemRequest);
	            
	            // Check the response.
	            System.out.println("Printing item after conditional update to new attribute...");
	            System.out.println(uResult.getAttributes());            
	            }
	      return IsRead;
    }

}
