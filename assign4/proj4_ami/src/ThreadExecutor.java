

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class ThreadExecutor {
	public static ExecutorService executor;
	public static String RESURL="https://sqs.us-west-2.amazonaws.com/521499112865/ResultQueue";
	
	public static void init(int n) {
		System.out.println("Initiating Executor Service");
		System.out.println("threads"+n);
		executor = Executors.newFixedThreadPool(n);
	}

public static HashMap<Integer, Integer> ExecuteTasks(HashMap<String , Integer> tasks) throws UnknownHostException{
	final HashMap<Integer, Integer> results = new HashMap<Integer, Integer>();
	
		for(Entry<String, Integer> entry: tasks.entrySet()){
			String task=entry.getKey();
			String[] taskC=task.split(" "); 
			final long time=Integer.parseInt(taskC[1]);
			System.out.println("Executing task "+entry.getValue());
			final int tasknum=entry.getValue();
			InetAddress IP=InetAddress.getLocalHost();
			final String ip=IP.getHostAddress();
			Future te=executor.submit(new Runnable(){
			
				@Override
				public void run() {
					// TODO Auto-generated method stub
					try {
						Thread.sleep(time);
						AWSSQS.sendMessage(RESURL, "Task number"+tasknum+" Result : Success");
						results.put(tasknum, 0);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						AWSSQS.sendMessage(RESURL, "Task number"+tasknum+" Result : Failed");
						results.put(tasknum, 1);
					}
					
				}
				
				
			});
			
		}
		
		return results;
		
	}

public static void StopExec() {
	System.out.println("Shutting Down Executor");
	executor.shutdown();
}
	
}
