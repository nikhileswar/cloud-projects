package com.proj3.gae.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;


@SuppressWarnings("serial")
public class cachesizeelem extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		 MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
		 res.setContentType("text/plain");
		 res.getWriter().println("Number of elements in cache = " + cache.getStatistics().getItemCount()); 
	
	}
}
		 
