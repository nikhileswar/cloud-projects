package com.proj3.gae.servlets;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.charset.Charset;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import com.google.appengine.tools.cloudstorage.*;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
public class findinfile extends HttpServlet {
	private final GcsService gcsService = GcsServiceFactory
			.createGcsService(RetryParams.getDefaultInstance());
    private MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("text/plain");
		String strValue=req.getParameter("key1");
		String strFilename=req.getParameter("key");
		String strFileContent="";
		res.getWriter().println("\n");
		res.getWriter().println("Find content in file");
		res.getWriter().println("\n");
		ListResult resultFiles = gcsService.list("gaebucket",
				ListOptions.DEFAULT);
		
		while (resultFiles.hasNext()) {
			ListItem listFile = resultFiles.next();
			String nameFile = listFile.getName().toString();
			if(nameFile.toLowerCase().contains(strFilename.toLowerCase()))
			{
				GcsFilename GCfilename = new GcsFilename("gaebucket", nameFile);
				GcsInputChannel inputchannel = gcsService.openReadChannel(GCfilename, 0);
				int fileSize = (int) gcsService.getMetadata(GCfilename).getLength();
				ByteBuffer result = ByteBuffer.allocate(fileSize);
				try (GcsInputChannel readChannel = gcsService.openReadChannel(GCfilename, 0)) {
					  readChannel.read(result);
					}
				inputchannel.close();
				byte[] bArray=new byte[fileSize];
				bArray=result.array();
				String strstr=new String(bArray);
				boolean found=strstr.toLowerCase().contains(strValue.toLowerCase());
				int no_occ=strstr.split(strValue).length - 1;
				for(int i=0;i<no_occ;i++){
					res.getWriter().println("found");
				}
			
			}
		}
		res.getWriter().println("\n");
		res.getWriter().println("Find content in file completed");
		res.getWriter().println("\n");
	}


}
